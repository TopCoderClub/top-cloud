package club.topcoder.top.topcloud;

import com.example.blog.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class IndexController {

    @GetMapping("/")
    public String index() {
        User user = new User("aa", "bb", "cc", "good boy", 1L);
        return LocalDateTime.now().toString() + "" + user.toString();
    }
}
