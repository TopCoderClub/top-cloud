package club.topcoder.top.topcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TopCloudApplication {

	public static void main(String[] args) {
		SpringApplication.run(TopCloudApplication.class, args);
	}

}
